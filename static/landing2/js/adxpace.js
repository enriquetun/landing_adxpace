(function($) {
    "use strict"; // Start of use strict
  
    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: (target.offset().top - 54)
          }, 1000, "easeInOutExpo");
          return false;
        }
      }
    });
  
    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function() {
      $('.navbar-collapse').collapse('hide');
    });
  
    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
      target: '#mainNav',
      offset: 56
    });
  
    // Collapse Navbar
    var navbarCollapse = function() {
      if ($("#mainNav").offset().top > 100) {
        $("#mainNav").addClass("navbar-shrink");
      } else {
        $("#mainNav").removeClass("navbar-shrink");
      }
    };
    // Collapse now if page is not at top
    navbarCollapse();
    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);
  
    // Hide navbar when modals trigger
    $('.portfolio-modal').on('show.bs.modal', function(e) {
      $('.navbar').addClass('d-none');
    })
    $('.portfolio-modal').on('hidden.bs.modal', function(e) {
      $('.navbar').removeClass('d-none');
    })

    // customized code for background size full height
    $('.banner').css({'height': (($(window).height()))+'px'});

    // alert($('.banner').css('height'));
 
    $(window).on('resize', function(){
    $('.banner').css({'height': (($(window).height()))+'px'});
    });

    //listen when collapsed the accordion so change awesomeicon
    $("#fb-accordion").on("hide.bs.collapse show.bs.collapse", function (e) {
      $(e.target)
        .prev()
        .find("i:last-child")
        .toggleClass("fa-angle-down fa-angle-up");
    });

    // functions for the search panels into the filters Searchs's module
    var arrayInputSearchs = {
      ddinputpais : ['ddpais'],
      ddinputciudad : ['ddciudad'],
      ddinputzona : ['ddzona'],
      ddinputmunicipio : ['ddmunicipio'],
      ddinputcolonia :['ddcolonia']
    };

    $.each(arrayInputSearchs,function(k,v) {
      $('#' + k).keyup(function() {
        filterFunction('#' + k,'#' + v[0]);
      });
    });

   var filterFunction = function (inputSearch,panelSearch) {
      var input, filter, ul, li, a, i, txtValue, div;
      input = $(inputSearch);
      filter = input.val().toUpperCase();
      div = [];
      div = $(panelSearch).find('a');
      for (i = 0; i < div.length; i++) {
        txtValue = div[i].textContent || div[i].innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          div[i].style.display = "";
        } else {
          div[i].style.display = "none";
        }
      }
    }

  })(jQuery); // End of use strict
  

  // Metodos Nuevos
  $("#btnClick").click(function(){
    $("#list-ocult").toggle("slow");
  });

  $("#btnClickEspacio").click(function(){
    $("#list-ocult-espacio").toggle("slow");
  });

  $("#btnClickGiro").click(function(){
    $("#list-ocult-giro").toggle("slow");
  });